#!/usr/bin/python python3

"""
============================================
Hofstadter's q-sequence.
Written in Python 3.4.3
============================================
"""

# Imports
# ----------------

from distutils.core import setup

# ----------------

__author__ = 'Gerold Baier'
__copyright__ = 'Copyright 2015'
__credits__ = ['Gerold Baier',
               'Pouria Hadjibagheri']
__license__ = 'GPLv.2'
__version__ = '0.3'
__maintainer__ = ['Gerold Baier']
__email__ = 'g.baier@ucl.ac.uk'
__date__ = '22/09/15, 18:34'

# -------------------------------------------------------


setup(
    name='Hofstadter',
    version='0.3-dev',

    packages=['src', 'build.lib'],

    package_data={
        # Include the licence file
        '': ['Licence.txt'],

        # Include other accompanying files.
        'src': [
            'requirements.txt',
                'hofstadter.spec'
        ],
    },

    # metadata for upload to PyPI
    url='',
    license='GPL-v.2',
    author='Gerold Baier, Pouria Hadjibagheri',
    author_email=[
        'Gerold Baier <g.baier@ucl.ac.uk>',
        'Pouria Hadjibagheri <p.bagheri.12@ucl.ac.uk>'
    ],
    keywords=[
        'Hofstadter',
        'Q-Sequence',
        'Eternal Golden Braid',
        'Mathematics',
    ],

    description="""
    Hofstadter's q-sequence, as described in:
        - Hofstadter, D. R. Gödel, Escher, Bach: An Eternal Golden Braid.
          New York: Vintage Books, p. 73, 1989.
    """,
)
