#!/usr/bin/python python3

"""
============================================
Hofstadter's q-sequence.
Written in Python 3.4.3
============================================
"""
# Imports
# ----------------
# NONE
# ----------------

__author__ = 'Gerold Baier'
__copyright__ = 'Copyright 2015'
__credits__ = ['Gerold Baier',
               'Pouria Hadjibagheri']
__license__ = 'GPLv.2'
__version__ = '0.3'
__maintainer__ = ['Gerold Baier']
__email__ = 'g.baier@ucl.ac.uk'
__date__ = '22/09/15, 18:34'

# -------------------------------------------------------
