#!/usr/bin/python python3

"""
============================================
Hofstadter's q-sequence.
Written in Python 3.4.3
============================================
"""

__author__ = 'Gerold Baier'
__copyright__ = 'Copyright 2015'
__credits__ = ['Gerold Baier',
               'Pouria Hadjibagheri']
__license__ = 'GPLv.2'
__version__ = '0.3'
__maintainer__ = ['Gerold Baier']
__email__ = 'g.baier@ucl.ac.uk'
__date__ = '22/09/15, 20:40'

# -------------------------------------------------------


def start():
    """
    Gets options from the user.
    :return: time, max_num, linear
    :rtype: bool, int, bool
    """
    time = None
    max_num = None
    response = None
    linear = None
    separator = 60
    print('\n')
    print('##'*separator)
    while True:
        print('-'*separator)

        # User: Time the programme, or not.
        try:
            if time is None:
                response = input(
                    '\nRun the timer:\n'
                    '==================\n'
                    'Warning: Running the timer will dobule the execution time.\n'
                    'Would you run the timer [y / n (default)]? '
                )
                if (response == '' or
                        response.capitalize() == 'N'):
                    time = False
                elif response.capitalize() == 'Y':
                    time = True
                else:
                    raise ValueError
        except ValueError:
            message = (
                '\n> ERROR: \n'
                '   Expected an <y> for "Yes" or <n> for "No", got <{}> instead.\n'
                .format(response)
            )
            print(message)
            time = None
            continue

        print('-'*separator)

        # User: Confirm the default maximum (100) or enter a new one.
        try:
            if max_num is None:
                response = None
                max_num_default = 100
                response = input(
                    '\nEnter the maximum (default={default}): '
                    .format(default=max_num_default)
                )
                if response.isnumeric():
                    max_num = int(response)
                elif response is '':
                    max_num = max_num_default
                else:
                    raise ValueError
        except ValueError:
            message = (
                '> ERROR: \n'
                '        Expected an integer, got <{}> instead.\n\n'
                .format(response)
            )
            print(message)
            max_num = None
            continue

        print('-'*separator)

        # User: Alternative plotting, or not.
        try:
            if linear is None:
                response = None
                linear_default = False
                response = input(
                    '\nWould you like the results to be illustrated as \n'
                    '   Q(n) - n/2 \n'
                    'on the graph [y / n (default)]? '
                )
                if (response == '' or
                        response.capitalize() == 'N'):
                    linear = linear_default
                elif response.capitalize() == 'Y':
                    linear = True
                else:
                    raise ValueError
        except ValueError:
            message = (
                '> ERROR: \n'
                '        Expected an integer, got <{}> instead.\n\n'
                .format(response)
            )
            print(message)
            linear = None
            continue

        if (time and
                max_num and
                linear) is not None:
            return time, max_num, linear
