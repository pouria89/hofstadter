#!/usr/bin/python python3

"""
========================================================================================
Hofstadter's q-sequence, as described in:
    - Hofstadter, D. R. Gödel, Escher, Bach: An Eternal Golden Braid.
      New York: Vintage Books, p. 73, 1989.

Programmed in Python 3.4.3
========================================================================================
"""

# Imports
# ----------------
from timeit import timeit

from matplotlib.pyplot import (
    plot, show, style, figure)


# ----------------

__author__ = 'Gerold Baier'
__copyright__ = 'Copyright 2015'
__credits__ = ['Gerold Baier',
               'Pouria Hadjibagheri']
__license__ = 'GPLv.2'
__version__ = '0.3'
__maintainer__ = ['Gerold Baier']
__email__ = 'g.baier@ucl.ac.uk'
__date__ = '22/09/15, 21:53'

# -------------------------------------------------------

# R style plots.
style.use('ggplot')


def run(num):
    """
    Function to calculate Hofstadter's Q-Sequence.
    :param num: Maximum number.
    :type num: int
    :return: counter = list of numbers from 0 to <num>.
             hofstadter = list of Hofstadter's q-sequence with respect to x.
    :rtype: list, list
    """
    hofs_num = [0, 1, 1]

    # +1 to make counter inclusive.
    counter = list(range(1, num+1))

    for __ in counter:
        # print('{:3}{:3}{}{:4}'.format(hofs_num[-1], hofs_num[-hofs_num[-1]], hofs_num, -hofs_num[-1]))
        calc_a = hofs_num[-hofs_num[-1]]
        calc_b = hofs_num[-hofs_num[-2]]
        hofs_num.append(calc_a+calc_b)

    # a = list()
    # [a.append(hofs_num[i] - item/2) for i, item in enumerate(counter)]
    hofstadter = hofs_num[3:]
    return counter, hofstadter


def linear(hofstadter):
    """
    Plots a Hofstadter sequence as: Q(n)-n/2
    :param hofstadter: Hofstadter sequence.
    :type hofstadter: list
    :return: cnt = list of numbers from 0 to the l.
             hof = list of Hofstadter's q-sequence with respect to x.
    :rtype: list, list
    """
    cnt = list(range(len(hofstadter)))
    lin = [hofstadter[i] - item/2 for i, item in enumerate(cnt)]
    return cnt, lin


if __name__ == '__main__':
    try:
        from src.intro import start
        time, max_num, lin = start()

        if time:
            arg_setup = 'from hofstadter import run'
            arg_func = 'run({num})'.format(num=max_num)

            timer = timeit(arg_func, setup=arg_setup, number=1)
            print('='*60, end='\n')
            print('\n'
                  '>> The programme took {val} seconds to run.'
                  .format(val=round(timer, 10)))

        x, hof = run(max_num)

        if lin:
            plt = figure()
            plt.add_subplot(2, 1, 1)
            plot(x, hof)

            lin_x, lin_y = linear(hof)
            plt.add_subplot(2, 1, 2)
            plot(lin_x, lin_y)
        else:
            plot(x, hof)

        show()
        print('\n'
              'Done.\n'
              'Good Bye...\n')
    except KeyboardInterrupt:
        print('\n'
              'The programme was aborted by the user.\n'
              'Good Bye...\n')

